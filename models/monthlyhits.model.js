const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let monthlyHitsSchema = new Schema({ 
    startDate : { type: Date },
    endDate : { type: Date },
    monthNumber : {type : Number},
    pageId : { type : mongoose.Schema.Types.ObjectId }, 
    title: { type: String }, 
    publishDate : { type: Date },
    authors: { type: String },
    type: { type: String },
    versions : { type : String },
    scopes : { type: String } 
}); 

module.exports = mongoose.model('MonthlyHits', monthlyHitsSchema);