const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let PageSchema = new Schema({ 
    url : { type : String, required: true },
    title : { type : String}, 
    hash : { type : String },
    versions : { type : String } , // { version : 1 , date : dd-mm-yyyy , file : pathtofile }
    publishdate : { type : Date},
    authors : { type : String } ,
    type : { type : String }
 
});


module.exports = mongoose.model('Page', PageSchema);