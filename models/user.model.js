const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let UserSchema = new Schema({ 
    cookieID : {type: String, required: true },
    ip : {type: String }, 
    country : {type : String}
});
 
module.exports = mongoose.model('User', UserSchema);