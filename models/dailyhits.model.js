const mongoose = require('mongoose');


function createDailyHitsSchema ()  {
    const Schema = mongoose.Schema;
        return new Schema({ 
        startDate : { type: Date },
        endDate : { type: Date },
        dayNumber : {type : Number},
        pageId : { type : mongoose.Schema.Types.ObjectId }, 
        title: { type: String }, 
        publishDate : { type: Date },
        authors: { type: String },
        type: { type: String },
        versions : { type : String },
        scopes : { type: String } 
    }); 

}

module.exports = mongoose.model('DailyHits', createDailyHitsSchema());