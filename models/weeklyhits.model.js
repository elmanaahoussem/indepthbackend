const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let weeklyHitsSchema = new Schema({ 
    startDate : { type: Date },
    endDate : { type: Date },
    weekNumber : {type : mongoose.Schema.Types.Number} ,
    pageId : { type : mongoose.Schema.Types.ObjectId }, 
    title: { type: String }, 
    publishDate : { type: Date },
    authors: { type: String },
    type: { type: String },
    versions : { type : String },
    scopes : { type: String } 
}); 

module.exports = mongoose.model('WeeklyHits', weeklyHitsSchema);