const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let BehaviorSchema = new Schema({ 
    visitId : {
        type : mongoose.Schema.Types.ObjectId,
        ref: 'Visit'
    },
    event: {type: String, required: true }, 
    timestamp : {type: Date },
    element: {type: String },
    value: {type: String }
}); 

module.exports = mongoose.model('Behavior', BehaviorSchema);