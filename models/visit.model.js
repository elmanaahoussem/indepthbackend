const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let VisitSchema = new Schema({ 
    userId :  {
        type : mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },  
    pageId:  {
        type : mongoose.Schema.Types.ObjectId,
        ref: 'Page'
    },  
    device: {type: String },
    windowHeight : {type: String} ,
    windowWidth : {type : String},
    ip : {type : String},
    referrer : {type : String},
    date : {type: Date, required: true } ,
    nexturl : {type:String}
});
 
module.exports = mongoose.model('Visit', VisitSchema);