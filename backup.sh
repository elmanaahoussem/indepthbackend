#!/bin/sh
docker-compose exec -T mongo mongodump --archive --gzip --db indepth > /home/debian/dumpsMongo/dump.gz

mv /home/debian/dumpsMongo/dump.gz /home/debian/dumpsMongo/$(date -d "today" +"%Y%m%d%H%M")dump.gz