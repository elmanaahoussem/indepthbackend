FROM node:12.14.1
WORKDIR /usr/app
COPY package.json .
EXPOSE 3000
RUN npm install
COPY . . 
