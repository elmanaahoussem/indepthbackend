#!/bin/sh
sudo docker exec -t stalker_indepth_mongo_1 mongodump --archive --gzip --db indepth > /home/debian/dumpsMongo/indepth-backup-$(date -d "today" +"%Y%m%d%H%M").gz
