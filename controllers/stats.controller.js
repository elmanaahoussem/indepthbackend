const Visit = require('../models/visit.model')
const User = require('../models/user.model')
const Page = require('../models/page.model')
const Behavior = require ('../models/behavior.model')
const fs = require('fs');
const {
    performance,
    PerformanceObserver
  } = require('perf_hooks');
const statsUser = require('./statsModules/users');
const statsPages = require('./statsModules/pages');
const { count } = require('console');

async function get_avg_readtime (req,pageInstance) {
    //Gets Avg Read Time in date range for N Pages  
        pagesVisits = await Visit.find({ 
            pageId: { $in: pageInstance }, 
            date : { $gte: req.query.startDate , $lte:  req.query.endDate },
            ip : { $nin :  req.query.banInkyteam == "true" ? ["41.226.164.41","51.210.179.152"]:  ["0"]}   
        })
 
    const behaviors = await Behavior.aggregate( [ 
        {
            $match: {  
                $and: [ { visitId: { $in : pagesVisits.map((elem) => elem._id) } }, { event: "articleOnFocus"   } ] 
            } 
        },
        {
            $group :
                { _id : "$visitId" ,
                  sumreadtime : { $sum: { '$convert': { 'input': '$value', 'to': 'int' } } },  
                } 
        },
        { 
            $group :
                { _id : null ,
                  avgreadtime : { $avg: '$sumreadtime' }
                } 
        }
    ])

    if (behaviors[0])
        return Math.ceil(behaviors[0].avgreadtime) 
    else 
        return 0

}

async function getChart_avg_readtime (req,pageInstance) {
    //Gets the number of Daily Unique Visitors/users in date range for N Pages  
    const pageVisits = await Visit.find({ pageId: { $in: pageInstance } , date : { $gte: req.query.startDate , $lte:  req.query.endDate } })
    let usersVisits = await Behavior.aggregate([
    {
        $match: {
            $and: [ { 
                event: "articleOnFocus" 
            } , 
            { 
                visitId: { $in : pageVisits.map((elem) => elem._id) } 
            },
            { ip : { $nin :  req.query.banInkyteam == "true" ? ["41.226.164.41","51.210.179.152"]:  ["0"] } } 
        ] 
        }
    },
    { 
        $group : { 
            _id : {
                year : { $year : "$timestamp" },        
                month : { $month : "$timestamp" },        
                day : { $dayOfMonth : "$timestamp" }
            } , 
            count : { $avg:  { '$convert': { 'input': '$value', 'to': 'int' } }},  
        } 
    }
    ]) 
    usersVisits = usersVisits.map( (e)  => {
        e.date = new Date( e._id.year + '-' +  e._id.month + '-' + e._id.day).getTime()
        return e
    })

    return usersVisits 
}

async function get_top_media (req,pageInstance) {
    // Get Top Media based on play time in date range for N Pages 
    const pagesVisits = await Visit.find({ pageId: { $in: pageInstance } , date : { $gte: req.query.startDate , $lte:  req.query.endDate } })
    let behaviors = await Behavior.aggregate( [ 
        {
            $match: {  
                $and: [ { visitId: { $in : pagesVisits.map((elem) => elem._id) } }, {event: "playTime"} ] 
            } 
        }
    ]) 
    await Visit.populate(behaviors,{path:'visitId',select:'pageId'}) 
    await Page.populate(behaviors,{path:'visitId.pageId',select:'url'}) 
  
    let topVideos = []
    let topAudios = []
   
    behaviors = behaviors.map((element) => { 
        element.element = JSON.parse(element.element)
        let newelement = new Object()
        newelement.trackId = element.element.trackId
        newelement.tagname = element.element.tagName
        newelement.title = element.element.title
        newelement.src = element.element.src
        newelement.value = parseInt(element.value)
        newelement.visitId = element.visitId.pageId.url
        return newelement
    })

    behaviors.map ((behaviorelement) => {
        if (behaviorelement.tagname == "VIDEO")
        index =  topVideos.findIndex( (element) => {  
        return ( element.trackId == behaviorelement.trackId && element.visitId == element.visitId )  
        })
        else 
        index =  topAudios.findIndex( (element) => {  
        return ( element.trackId == behaviorelement.trackId && element.visitId == element.visitId )  
        })
        
        if (index != -1 )
        {
            if (behaviorelement.tagname == "VIDEO")
                topVideos[index].value += behaviorelement.value
            else 
                topAudios[index].value += behaviorelement.value
        }else {
            if (behaviorelement.tagname == "VIDEO")
                topVideos.push(behaviorelement)
            else
                topAudios.push(behaviorelement)
        }
    })
    return {topAudios:topAudios.slice(0, req.query.numberOfItems).map((e) => e) , topVideos:topVideos.slice(0, req.query.numberOfItems).map((e) => e)}  
    

}

async function get_bounce_rate (req,pageInstance) { 

    let  reslt = await Visit.aggregate([
        {
            $match: {
                $and: [ 
                    { date: { $gte: new Date(req.query.startDate), $lte: new Date( req.query.endDate) } }, 
                    { pageId: { $in : pageInstance.map((elem) => elem._id) } },
                    { ip: { $nin :  req.query.banInkyteam == "true" ? ["41.226.164.41","51.210.179.152"]:  ["0"] } }
                ] 
            }
        } ,
        { 
            $group :{ 
                _id : "$referrer",
                refcount : { $sum: 1 },  
            } 
        }, 
        {
            $sort : { "refcount" : -1 }
        } 
        ]) 
    
    let urls = [] 
    let sumbouncing = 0
    await Promise.all( reslt.map(async function (e)  {
        if (e._id) 
        if ( e._id.indexOf('https://inkyfada.com') == 0) {
            let pageInstance = await Page.find({ url: e._id })
            let visits = await Visit.find({ pageId: pageInstance })
            e.visitcount = visits.length 
            e.boucerate = 100 -  ( e.refcount * 100 / visits.length ) 
            if (e.boucerate >= 0) {
                urls.push(e) 
                sumbouncing += e.boucerate
            }
        } 
    })) 
 
    return {topPages : urls.sort((a,b) => a.boucerate - b.boucerate ).splice(0,10).map((e) => e) , bounceRate : Math.ceil( sumbouncing / urls.length)}
}

async function get_reading_time_section (req,pageInstance) {
    // calcule le nombre des utilisateurs par palier
    const pageVisits = await Visit.find({ pageId: { $in: pageInstance } ,
         date : { $gte: req.query.startDate , $lte:  req.query.endDate }  , 
         ip : { $nin :  req.query.banInkyteam == "true" ? ["41.226.164.41","51.210.179.152"]:  ["0"]} } )
    const behaviors = await Behavior.aggregate( [ 
        {
            $match: {  
                $and: [ { visitId: { $in : pageVisits.map((elem) => elem._id) } }, { event: "articleOnFocus"   } ] 
            } 
        },
        { 
            $group :
                { _id : "$visitId" ,
                  sumreadtime : { $sum: { '$convert': { 'input': '$value', 'to': 'int' } } },    
                } 
        } 
    
    ])
    await Visit.populate(behaviors, {path: '_id',select : 'userId'}) 
    let rest = []
    behaviors.map((e)=> { 
        let item = new Object()
        item.userId = e._id.userId
        item.visitTime = e.sumreadtime
        item.visitsNum = 1
        item.avg = e.sumreadtime

        let indexUser = rest.findIndex((elm) =>  String(elm.userId).trim() == String(item.userId).trim() )
        
        if (indexUser != -1) {
            rest[indexUser].visitTime += item.visitTime
            rest[indexUser].visitsNum ++
            rest[indexUser].avg = rest[indexUser].visitTime / rest[indexUser].visitsNum
        }
        else 
        {
            rest.push(item) 
        } 
    })

    let counting = {
        a15:0,
        a30:0,
        a45:0,
        a60:0,
        a90:0
    }
    rest.map((e)=> {
        if (parseInt(e.avg) >= 90 ){
                counting.a15 += 1
                counting.a30 += 1  
                counting.a45 += 1
                counting.a60 += 1  
                counting.a90 += 1 
        } else if ( parseInt(e.avg) >= 60 ) {
                counting.a15 += 1
                counting.a30 += 1  
                counting.a45 += 1
                counting.a60 += 1  
        } else if (parseInt(e.avg) >= 45  ) {
                counting.a15 += 1
                counting.a30 += 1  
                counting.a45 += 1 
        } else if ( parseInt(e.avg) >= 30 ) {
                counting.a15 += 1
                counting.a30 += 1   
        } else if ( parseInt(e.avg) >= 15  ) {
                counting.a15 += 1  
        }
         
    })

    counting.a15 = Math.ceil(counting.a15 * 100 / rest.length)
    counting.a30 = Math.ceil(counting.a30 * 100 / rest.length)
    counting.a45 = Math.ceil(counting.a45 * 100 / rest.length)
    counting.a60 = Math.ceil(counting.a60 * 100 / rest.length)
    counting.a90 = Math.ceil(counting.a90 * 100 / rest.length)

    return counting
}

async function get_top_authors (req) { 
    //Gets Top 10 pages based on visits in date range for N Pages
    const urls = req.query.url  ? JSON.parse(req.query.url) : [] 
    const pageInstance = urls.length >= 1 ? await Page.find({ url:  { $in:  urls } , type : 'single' }) : await Page.find({type : 'single'}) 
    const toppages = await Visit.aggregate([
    {
        $match: {
            $and: [ { date: { $gte: new Date(req.query.startDate), $lte: new Date( req.query.endDate) } } ,
                 { pageId: { $in : pageInstance.map((elem) => elem._id) } } ,
                 { ip : { $nin :  req.query.banInkyteam == "true" ? ["41.226.164.41","51.210.179.152"]:  ["0"]} } ] 
        }
    },

    { 
        $group :{ 
            _id : "$pageId" ,
            visitcount : { $sum: 1 },  
        } 
    },
    {
        $sort : { "visitcount" : -1 }
    }
   ]) 
   const toppopulated = await Page.populate(toppages, {path: '_id'})

   let rest = []
    toppopulated.map((e)=> { 
        let item = new Object()
        item.authors = e._id.authors
        item.visitcount = e.visitcount 

        let indexUser = rest.findIndex((elm) =>  String(elm.authors).trim() == String(item.authors).trim() )
        
        if (indexUser != -1) {
            rest[indexUser].visitcount += item.visitcount 
        }
        else 
        {
            rest.push(item) 
        }  
    }) 
    rest = rest.sort((a,b) => b.visitcount - a.visitcount) 
    return rest.slice(0, req.query.numberOfItems).map((e) => e) 
}

 
exports.get_stats = async function ( req,res,next) {  
    // Calculates the States and returns all metrics  
    let cacheQueries = JSON.parse(fs.readFileSync('logs/queries.json',{encoding:'utf8', flag:'r'})) 
    let IndexOfQuery = cacheQueries.findIndex( (e) => JSON.stringify(e.query) == JSON.stringify(req.query) ) 
    if (IndexOfQuery != -1)
    return res.json(cacheQueries[IndexOfQuery].result)  
   
    let p1 = performance.now()
    let response = new Object()
    const urls = req.query.url  ? JSON.parse(req.query.url) : [] 
    const pageInstance = urls.length >= 1 ? await Page.find({ url:  { $in:  urls }  }) : await Page.find({})  
    response.numberVisits = await statsUser.get_number_visits(req,pageInstance)  
    response.numberUniqueUsers = await statsUser.get_number_users(req,pageInstance)
    response.avgReadTime = await get_avg_readtime(req,pageInstance)
    response.topPagesByVisits = await statsPages.get_top_pages_by_visits(req,pageInstance)
    response.topUsersByVisits = await statsUser.get_top_users_by_visits(req,pageInstance)
    response.topPagesByReadtime = await statsPages.get_top_pages_by_readtime(req,pageInstance)
    response.topMedias = await get_top_media(req,pageInstance) 
    let usersvisits = await statsUser.get_users_visits(req,pageInstance)
    response.numberNewUsers = usersvisits.filter((e) => e.visitcount == 1).length
    response.numberReturningUsers = usersvisits.filter((e) => e.visitcount > 1).length
    response.numberLoyalUsers = usersvisits.filter((e) => e.visitcount > 4).length 
    //response.bounceRate = await get_bounce_rate(req,pageInstance)
    response.topPosts = await statsPages.get_top_posts(req)
    response.topAuthors = await get_top_authors(req)
    response.readingTimeSections = await get_reading_time_section(req,pageInstance)
    response.scrollPalier = await statsUser.get_users_scroll_sections(req,pageInstance)
    response.usersByDevice = await statsUser.getUsersByDevices(req,pageInstance)
    response.usersByRef = await statsUser.getUsersNumberByRef(req,pageInstance)
    response.usersByCountry = await statsUser.getUsersNumberByCountry(req,pageInstance)  
    response.chartVisitsAndUsers = await statsUser.getChart_Number_users(req,pageInstance)
    response.chartAvgReadTime =  await getChart_avg_readtime(req,pageInstance)
    response.tableauTopPages = await get_top_pages_withInfo(req,pageInstance)

    response.classify_traffic = await classify_traffic(req,pageInstance) //new
    response.chartUsersStacked = await statsUser.getChart_users_stacked(req,pageInstance) // new
    response.chartAvgDepth = await statsUser.getChart_avg_depth(req,pageInstance) // new

    let p2 = performance.now()
    console.log('TIMING OF QUERIES' , p2-p1)
  
    cacheQueries.push({ query : req.query , result : response})
    fs.writeFileSync('logs/queries.json', JSON.stringify(cacheQueries))  
    
    res.json(response)
}

 async function get_top_pages_withInfo ( req,pageInstance ) {
   
    let toppages = await Visit.aggregate([
        {
            $match: {
                $and: [ { date: { $gte: new Date(req.query.startDate), $lte: new Date( req.query.endDate) } } ,
                    { pageId: { $in : pageInstance.map((elem) => elem._id) } } ,
                    { ip : { $nin :  req.query.banInkyteam == "true" ? ["41.226.164.41","51.210.179.152"]:  ["0"] } }   ] 
            }
        },
        { 
            $group :{ 
                _id : "$pageId" , 
                visitcount : { $sum: 1 },   
            } 
        },
        {
            $sort : { "visitcount" : -1 }
        }
       ]) 
    const toppopulated = await Page.populate(toppages, {path: '_id',select:"url"} ) 
    let response = await Promise.all( toppopulated.slice(0, req.query.numberOfItems).map( async (e) => {
        let pageInstance = null
        pageInstance =  await Page.find({ url:  String(e._id.url) })   
         
        if ( pageInstance.length > 0 ) {
            e.numberVisits = await statsUser.get_number_visits(req,pageInstance)  
            e.saissons = await statsUser.get_number_users(req,pageInstance)
            e.avgReadTime = await get_avg_readtime(req,pageInstance)
            e.depthRate = await statsUser.get_users_scroll_sections(req,pageInstance) 
        }
        return e
    }) )

    return response
}


async function classify_traffic (req,pageInstance) {
    let rslt = await Visit.aggregate([
        {
            $match: {
                $and: [ { date: { $gte: new Date(req.query.startDate), $lte: new Date( req.query.endDate) } } ,
                        { pageId: { $in : pageInstance.map((elem) => elem._id) } } ,
                        { ip : { $nin :  req.query.banInkyteam == "true" ? ["41.226.164.41","51.210.179.152"]:  ["0"] } }   
                    ] 
            }
        }   
       ]) 
      
    let classification = {
        organic : 0,
        social : 0 , 
        direct : 0 ,
        ref : 0 ,
        total : 0 
    }
    
    let organic = ["google", "bing", "duckduckgo", "ecosia", "yahoo", "s3arch", "startpage", "cbsearch", "ask.com", "search.myway", "search.lilo", "mybrowser-search", "search-7"]
    let social =  ["facebook.com", "t.co/", "instagram.com", "linkedin.com", "lnkd.in", "twitter", "messenger"]
    let mail = ['gmail','mail.yahoo']
    
    rslt.map((e) => {
         
        let isOther = true
        
        if ( ! e.referrer) {
            classification.total += 1
            classification.direct +=1 
        } else { 
        organic.forEach((o) => {
            if (e.referrer.indexOf(o) != -1){
                classification.total += 1
            classification.organic += 1
            isOther = false
            }
        }) 
        social.forEach((o) => {
            if (e.referrer.indexOf(o) != -1){
                classification.total += 1
            classification.social += 1
            isOther = false
            }
        })
        /*
        mail.forEach((o) => {
            if (e.referrer.indexOf(o) != -1) {
            classification.mail += 1
            isOther = false
            }
        })
        */  
        if (e.referrer.indexOf('https://inkyfada.com') != -1) {
            //classification.refIntern += 1
            isOther = false


        } 
         
        if (isOther) {
            classification.total += 1
            classification.ref +=1
         }

        }

    })
      
    return  classification 

} 

exports.apiTest = async function ( req,res,next) {  
    // EndPoint made to test Functions separately  
    const urls = req.query.url  ? JSON.parse(req.query.url) : [] 
    const pageInstance = urls.length >= 1 ? await Page.find({ url:  { $in:  urls }  }) : await Page.find({})  
    res.json(await classify_traffic(req,pageInstance)) 
}

exports.fixipvisit = async function ( req,res,next) {  
    // EndPoint made to test Functions separately  
    let visits = await Visit.find({})
    //console.log(visits)
    Promise.all(visits.map(async (e) => {
        let theUser = await User.findOne({_id : e.userId})
        e.ip = theUser.ip
        try {
            await Visit.updateOne({ _id: e._id}, {$set: e}) 
        } catch (err) {
            log.logError("Error Message : " + err.message)
            return next(err) 
        } 
        })
    ) 
    res.json('done')
}

exports.get_pagelist =  async function ( req,res,next) { 
    // Returns Page List
    const pages = await Page.find({}) 
    res.json(pages.map((e) => e.url))
}
 






