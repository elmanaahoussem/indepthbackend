const jwt = require('jsonwebtoken');
const hashCode = 'Xz5a989fasSead5vccea5'

exports.encode = function (data) {
  return jwt.sign( data , hashCode)
};

exports.decode = function (token) {
  try { 
    return  jwt.verify(token, hashCode);
  } catch(err) {
    return false;
  } 
};