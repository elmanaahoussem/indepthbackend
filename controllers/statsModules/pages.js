const Visit = require('../../models/visit.model')
const User = require('../../models/user.model')
const Page = require('../../models/page.model')
const Behavior = require ('../../models/behavior.model')

exports.get_top_pages_by_visits = async function (req,pageInstance) {
    //Gets Top 10 pages based on visits in date range for N Pages 
    const toppages = await Visit.aggregate([
    {
        $match: {
            $and: [ { date: { $gte: new Date(req.query.startDate), $lte: new Date( req.query.endDate) } } ,
                    { pageId: { $in : pageInstance.map((elem) => elem._id) } },
                    { ip : {$nin :  req.query.banInkyteam == "true" ? ["41.226.164.41","51.210.179.152"]:  ["0"]} }   
                ] 
        }
    },
    {
        $group :{ 
            _id : "$pageId" ,
            visitcount : { $sum: 1 },  
        } 
    },
    {
        $sort : { "visitcount" : -1 }
    }
   ]) 
   let toppopulated = await Page.populate(toppages, {path: '_id',select: "title"  } )
   //await Page.populate(toppages, {path: '_id',select: "title"} )
   return toppopulated.slice(0, req.query.numberOfItems? req.query.numberOfItems : 10 ).map((e) => e)
}

exports.get_top_pages_by_readtime = async function (req,pageInstance) {
    //Gets Top 10 pages based on readTime in date range for N Pages
    
    let pagesVisits = await Visit.find({ pageId: { $in: pageInstance } , date : { $gte: req.query.startDate , $lte:  req.query.endDate } ,  ip : { $nin :  req.query.banInkyteam == "true" ? ["41.226.164.41","51.210.179.152"]:  ["0"] }    })
    const behaviors = await Behavior.aggregate([ 
        {
            $match: {  
                $and: [ { visitId: { $in : pagesVisits.map((elem) => elem._id) } }, { event: "articleOnFocus"   } ] 
            } 
        },
        { 
            $group :
                { _id : "$visitId" ,
                  sumreadtime : { $sum: { '$convert': { 'input': '$value', 'to': 'int' } } },    
                } 
        },
        {
            $sort : { "avgreadtime" : -1 }
        } 
    ])
    
    const toppopulated = await Visit.populate(behaviors,{path:'_id',select:'pageId'})
    let toppages = await Page.populate(toppopulated,{path:'_id.pageId' ,select:['url','title']})
     
    let topPages = [] 
    toppages.map((e) => {
        let item = new Object()
        item.url = e._id.pageId.url
        item.title = e._id.pageId.title
        item.sumreadtime = e.sumreadtime 
        item.count = 1
        elemIndex = topPages.findIndex((e) => e.url == item.url) 
        if (elemIndex != -1) {
            topPages[elemIndex].sumreadtime += item.sumreadtime
            topPages[elemIndex].count ++
        } else 
            topPages.push(item)
    })
    
    topPages = topPages.sort((a,b) => { 
        return (b.sumreadtime / b.count) - (a.sumreadtime / a.count)
    })    

    return topPages.slice(0, req.query.numberOfItems? req.query.numberOfItems : 10 ).map((e) => e) 
}

exports.get_top_posts = async function (req) { 
    
    const urls = req.query.url  ? JSON.parse(req.query.url) : [] 
    let pageInstance ;
    if ( req.query.startPublishDate && req.query.endPublishDate )
    {
         pageInstance = urls.length >= 1 ? await Page.find({ url:  { $in:  urls } , type : 'single' , publishdate : { $gte: new Date(req.query.startPublishDate) , $lte: new Date( req.query.endPublishDate) } }) : await Page.find({ type : 'single' , publishdate : { $gte: new Date(req.query.startPublishDate), $lte: new Date( req.query.endPublishDate) }  })  
    }else {
         pageInstance = urls.length >= 1 ? await Page.find({ url:  { $in:  urls } , type : 'single'  }) : await Page.find({type : 'single'}) 
    } 
  
    let toppages = await Visit.aggregate([
    {
        $match: {
            $and: [ { date: { $gte: new Date(req.query.startDate), $lte: new Date( req.query.endDate) } } ,
             { pageId: { $in : pageInstance.map((elem) => elem._id) } } , 
             { ip : { $nin :  req.query.banInkyteam == "true" ? ["41.226.164.41","51.210.179.152"]:  ["0"] } }
               ] 
        }
    },
    { 
        $group :{ 
            _id : "$pageId" ,
            visitcount : { $sum: 1 },  
        } 
    },
    {
        $sort : { "visitcount" : -1 }
    }
   ])  
   const toppopulated = await Page.populate(toppages, {path: '_id' , select:["url","title"]})
   
   return toppopulated.slice(0, req.query.numberOfItems? req.query.numberOfItems : 10 ).map((e) => e) 

}

 

