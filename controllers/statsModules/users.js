const Visit = require('../../models/visit.model')
const User = require('../../models/user.model')
const Page = require('../../models/page.model')
const Behavior = require ('../../models/behavior.model')

exports.get_number_visits = async function (req,pageInstance) { 
    //Gets the number of visits in date range for N Pages    
    let pageVisits = await Visit.find({ 
        pageId: { $in: pageInstance } , 
        date : { $gte: req.query.startDate , $lte:  req.query.endDate } , 
        ip : { $nin :  req.query.banInkyteam == "true" ? ["41.226.164.41","51.210.179.152"] :  ["0"] }  
    })  
    return  pageVisits.length 
}

exports.get_number_users  = async function (req,pageInstance) {
     //Gets the number of Unique Visitors/users in date range for N Pages 
 
     let usersVisits = await Visit.find({ 
            pageId: { $in: pageInstance } , 
            date : { $gte: req.query.startDate , $lte:  req.query.endDate },
            ip : { $nin :  req.query.banInkyteam == "true" ? ["41.226.164.41","51.210.179.152"]:  ["0"]}  
     }).distinct('userId') 
     return usersVisits.length 
}

exports.get_top_users_by_visits = async function (req,pageInstance) {
    //Gets Top 10 Users based on visits in date range for N Pages 
    const topusers = await Visit.aggregate(
        [
            {
                $match: {
                    $and: [ { date: { $gte: new Date(req.query.startDate), $lte: new Date( req.query.endDate) } } , 
                            { pageId: { $in : pageInstance.map((elem) => elem._id) } } , 
                            { ip : { $nin :  req.query.banInkyteam == "true" ? ["41.226.164.41","51.210.179.152"]:  ["0"]} }   ] 
                }
            },
            { 
                $group :{ 
                    _id : "$userId",
                    visitcount : { $sum: 1 },  
                } 
            },
            {
                $sort : { "visitcount" : -1 }
            }
        ])

    const toppopulated = await User.populate(topusers, {path: '_id' , select:"cookieID"})
    return toppopulated.slice(0, req.query.numberOfItems? req.query.numberOfItems : 10 ).map((e) => e)
}

exports.get_users_visits = async function (req,pageInstance) {
    //Gets every User number of visits used to get New/returning/loyal users 
        let toppages = await Visit.aggregate([
        {
            $match: {
                $and: [
                    { date: { $gte: new Date(req.query.startDate), $lte: new Date( req.query.endDate) } } , 
                    { pageId: { $in : pageInstance.map((elem) => elem._id) } }  ,
                    { ip : {$nin :  req.query.banInkyteam == "true" ? ["41.226.164.41","51.210.179.152"]:  ["0"]} }
                ] 
            }
        },
        { 
            $group :{ 
                _id : "$userId",
                visitcount : { $sum: 1 },  
            } 
        },
        {
            $sort : { "visitcount" : -1 }
        }
        ])
 
    return await User.populate(toppages, {path: '_id'}) 
}

exports.getUsersByDevices = async function (req,pageInstance) {
    const visits = await Visit.aggregate([
        {
            $match: {
                $and: [ 
                    { date: { $gte: new Date(req.query.startDate), $lte: new Date( req.query.endDate) } } , 
                    { pageId: { $in : pageInstance.map((elem) => elem._id) } },
                    { ip : { $nin :  req.query.banInkyteam == "true" ? ["41.226.164.41","51.210.179.152"]:  ["0"]} }  
                ] 
            }
        },
        { 
            $group :{ 
                _id : "$device" ,
                visitcount : { $sum: 1 },  
            } 
        },
        {
            $sort : { "visitcount" : -1 }
        }
       ]) 

       return visits.splice(0,req.query.numberOfItems ? req.query.numberOfItems : 10)
}

exports.getUsersNumberByRef  = async function ( req,pageInstance ) {
    // gets the number of users by every Referral
    let usersVisits = await Visit.aggregate([
        {
            $match: {
                $and: [ 
                    { date: { $gte: new Date(req.query.startDate), $lte: new Date( req.query.endDate) } } , 
                    { pageId: { $in : pageInstance.map((elem) => elem._id) } } ,
                    { ip : { $nin :  req.query.banInkyteam == "true" ? ["41.226.164.41","51.210.179.152"]:  ["0"] } }  ] 
            }
        },
        { 
            $group :{ 
                _id : "$referrer" ,
                visitcount : { $sum: 1 },  
            } 
        },
        {
            $sort : { "visitcount" : -1 }
        }
       ]) 
       
    let rslt  = []

    usersVisits.map((e) => {
        let domain = new Object()
        if(e._id)
        domain.url = e._id.match('^(?:https?:\/\/)?(?:[^@\/\n]+@)?(?:www\.)?([^:\/?\n]+)')[0]
        else 
        domain.url = "direct"
        domain.refCount = e.visitcount
        
        let indexDomain = rslt.findIndex((e) => e.url == domain.url)
        if (indexDomain != -1 ) 
            rslt[indexDomain].refCount += domain.refCount
        else
            rslt.push(domain)
    })
 
    return (rslt.splice(0,10)) 
}

exports.getUsersNumberByCountry  = async function ( req,pageInstance ) { 
    // Get Number of users by country
    const usersVisits = await Visit.find({ 
        pageId: { $in: pageInstance } , 
        date : { $gte: req.query.startDate , $lte:  req.query.endDate } ,
        ip : { $nin :  req.query.banInkyteam == "true" ? ["41.226.164.41","51.210.179.152"]:  ["0"]}  
    }).distinct('userId')
    let users = await User.aggregate([
        {
            
            $match: {
                $and: [{ _id : { $in : usersVisits.map((elem) => elem) } }  ] 
            }
        } ,
        { 
            $group :{ 
                _id : "$country" ,
                visitcount : { $sum: 1 },  
            } 
        },
        {
            $sort : { "visitcount" : -1 }
        } 
       ])  

    return (users.splice(0,10)) 
}

exports.getChart_avg_depth = async function ( req,pageInstance) {
    const pageVisits = await Visit.find({
        pageId: { $in: pageInstance } ,
        date : { $gte: req.query.startDate , $lte:  req.query.endDate } ,  
        ip : { $nin :  req.query.banInkyteam == "true" ? ["41.226.164.41","51.210.179.152"]:  ["0"] }    
   }) 
   const behaviors = await Behavior.aggregate([ 
    {
        $match: {  
            $and: [ { visitId: { $in : pageVisits.map((elem) => elem._id) } }, { event: "scrollOnDocument" } ] 
        } 
    },
    { 
        $group :
            {   _id : {
                year : { $year : "$timestamp" },        
                month : { $month : "$timestamp" },        
                day : { $dayOfMonth : "$timestamp" }
            }  ,
              scroll : { $avg: { '$convert': { 'input': '$value', 'to': 'double' } } },    
            } 
    }   
    ])

    let scrolls = behaviors.map( (e)  => { 
        e.date = new Date( e._id.year + '-' +  e._id.month + '-' + e._id.day).getTime()
        return e
   })
    return scrolls
}

exports.getChart_bounceRate = async function (req, pageInstance) {
    let  reslt = await Visit.aggregate([
        {
            $match: {
                $and: [ 
                    { date: { $gte: new Date(req.query.startDate), $lte: new Date( req.query.endDate) } } , 
                    { pageId: { $in : pageInstance.map((elem) => elem._id) } }  ,
                    { ip : {$nin :  req.query.banInkyteam == "true" ? ["41.226.164.41","51.210.179.152"]:  ["0"]} }
                    ] 
            }
        } ,
        { 
            $group :{ 
                _id : { 
                    referrer : "$referrer",
                        
                    
                
                },
                refcount : { $sum: 1 },  
            } 
        }, 
        {
            $sort : { "refcount" : -1 }
        } 
        ]) 
    
    let urls = [] 
    let sumbouncing = 0
    await Promise.all( reslt.map(async function (e)  {
        if (e._id) 
        if ( e._id.indexOf('https://inkyfada.com') == 0) {
            let pageInstance = await Page.find({ url: e._id })
            let visits = await Visit.find({ pageId: pageInstance })
            e.visitcount = visits.length 
            e.boucerate = 100 -  ( e.refcount * 100 / visits.length ) 
            if (e.boucerate >= 0) {
                urls.push(e) 
                sumbouncing += e.boucerate
            }
        } 
    })) 
    return reslt
}

exports.get_users_scroll_sections  = async function ( req,pageInstance) {  
    // Gets % of users by their scroll on Pages  
    
    const pageVisits = await Visit.find({
         pageId: { $in: pageInstance } ,
         date : { $gte: req.query.startDate , $lte:  req.query.endDate } ,  
         ip : { $nin :  req.query.banInkyteam == "true" ? ["41.226.164.41","51.210.179.152"]:  ["0"] }    
    })
    const behaviors = await Behavior.aggregate( [ 
        {
            $match: {  
                $and: [ { visitId: { $in : pageVisits.map((elem) => elem._id) } }, { event: "scrollOnDocument"   } ] 
            } 
        },
        { 
            $group :
                { _id : "$visitId" ,
                  scroll : { $max: '$value' },    
                } 
        }  
    ])
    
    await Visit.populate(behaviors, {path: '_id',select : ['userId','pageId']}) 
    await Page.populate(behaviors, {path: '_id.pageId',select : ['url','title']}) 
    

    let reslt = {
        pages : [],
        users : { 
            a25 : 0,
            a50 : 0,
            a75 : 0,
            a90 : 0,
        } 
    }

    let usersavgs = []

    behaviors.map((e) => {
        let useritem = new Object() 
        useritem.userId = e._id.userId
        useritem.sumscroll = Math.ceil(parseFloat(e.scroll))
        useritem.visitcount = 1
        useritem.avgScroll = Math.ceil(parseFloat(e.scroll)) 

        let pageitem = new Object() 
        pageitem.pageId = e._id.pageId._id
        pageitem.title = e._id.pageId.title
        pageitem.sumscroll = Math.ceil(parseFloat(e.scroll))
        pageitem.url = e._id.pageId.url
        pageitem.visitcount = 1
        pageitem.avgScroll = Math.ceil(parseFloat(e.scroll)) 

        let indexOfPage = reslt.pages.findIndex( (e) => String(e.pageId).trim() ==  String(pageitem.pageId).trim() )
        let indexOfUser = usersavgs.findIndex( (e) => String(e.userId).trim() ==  String(useritem.userId).trim() )

        if (indexOfPage != -1 ) {
            reslt.pages[indexOfPage].sumscroll += pageitem.sumscroll
            reslt.pages[indexOfPage].visitcount++
            reslt.pages[indexOfPage].avgScroll = reslt.pages[indexOfPage].sumscroll / reslt.pages[indexOfPage].visitcount
        }else {
            reslt.pages.push(pageitem)
        }

        if (indexOfUser != -1 ) {
            usersavgs[indexOfUser].sumscroll += useritem.sumscroll
            usersavgs[indexOfUser].visitcount++
            usersavgs[indexOfUser].avgScroll = usersavgs[indexOfUser].sumscroll / usersavgs[indexOfUser].visitcount
        }else {
            usersavgs.push(useritem)
        }

    })

    reslt.pages = reslt.pages.sort((a,b) => b.avgScroll - a.avgScroll).slice(1, 10).map((e) => e)
    
    usersavgs.map((e)=> {
        if ( parseInt(e.avgScroll) >= 90 ){
                reslt.users.a25 += 1
                reslt.users.a50 += 1  
                reslt.users.a75 += 1
                reslt.users.a90 += 1   
        } else if ( parseInt(e.avgScroll) >= 75 ) {
                reslt.users.a25 += 1
                reslt.users.a50 += 1  
                reslt.users.a75 += 1 
        } else if (parseInt(e.avgScroll) >= 50  ) {
                reslt.users.a25 += 1
                reslt.users.a50 += 1   
        } else if ( parseInt(e.avgScroll) >= 25 ) {
                reslt.users.a25 += 1  
        }  
    })
    reslt.users.a25 = Math.ceil(reslt.users.a25 * 100 / usersavgs.length)
    reslt.users.a50 = Math.ceil(reslt.users.a50 * 100 / usersavgs.length)
    reslt.users.a75 = Math.ceil(reslt.users.a75 * 100 / usersavgs.length)
    reslt.users.a90 = Math.ceil(reslt.users.a90 * 100 / usersavgs.length)   
    return reslt
}

exports.getChart_Number_users = async function(req,pageInstance) { 
    //Gets the number of Daily Unique Visitors/users in date range for N Pages  
    let usersVisits = await Visit.aggregate([
        {
            $match: {
                $and: [ { 
                    date: { $gte: new Date(req.query.startDate), $lte: new Date( req.query.endDate) } 
                } , 
                { 
                    pageId: { $in : pageInstance.map((elem) => elem._id) } 
                } ,
                { ip : { $nin :  req.query.banInkyteam == "true" ? ["41.226.164.41","51.210.179.152"]:  ["0"]} } 
                ] 
            }
        },
        { 
            $group : { 
                _id : {
                    year : { $year : "$date" },        
                    month : { $month : "$date" },        
                    day : { $dayOfMonth : "$date" }
                } ,
                users: { $addToSet: "$userId" }, 
                count : { $sum: 1 },  
            } 
        } 
       ]) 
       usersVisits = usersVisits.map( (e)  => {
            e.users = e.users.length  
            e.date = new Date( e._id.year + '-' +  e._id.month + '-' + e._id.day).getTime()
            return e
       })
    return usersVisits 
}

exports.getChart_users_stacked = async function(req,pageInstance) { 
    //Gets the number of Daily Unique Visitors/users in date range for N Pages  
    let usersVisits = await Visit.aggregate([
        {
            $match: {
                $and: [ 
                {  date: { $gte: new Date(req.query.startDate), $lte: new Date( req.query.endDate) }  } , 
                {  pageId: { $in : pageInstance.map((elem) => elem._id) }  } ,
                {  ip : { $nin :  req.query.banInkyteam == "true" ? ["41.226.164.41","51.210.179.152"]:  ["0"] } } 
                ] 
            }
        },
        { 
            $group :{ 
                _id : {
                    year : { $year : "$date" },        
                    month : { $month : "$date" },        
                    day : { $dayOfMonth : "$date" },
                    userId : "$userId" 
                } 
                ,
                visitcount : { $sum: 1 },  
            } 
        }, 
       ]) 

       let newUsersChart = []
       let returningChart = [] 

       usersVisits = usersVisits.map( (e)  => {
            e.date = new Date( e._id.year + '-' +  e._id.month + '-' + e._id.day).getTime()
            if (e.visitcount == 1)
            {
                newUsersChart.push(e)
            } else if (e.visitcount > 1) {
                returningChart.push(e)
            }
            return e
       })
    return { newUsersChart:newUsersChart , returningChart:returningChart }
}
