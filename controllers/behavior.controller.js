const Behavior = require('../models/behavior.model');
const Visit = require('../models/visit.model');
const Logger = require('./Logger')


exports.add_behavior = async function (req, res,next) {   
    let log = new Logger ()  
    let behaviors = JSON.parse(req.body.behaviors)
    let visitId = behaviors.visitid
    let timestamp =  behaviors.timestamp
    let nexturl = behaviors.nexturl
     
    if (nexturl) {
        try {
            let visitInstance = await Visit.findOne({ _id : visitId })
            visitInstance.nexturl = nexturl
            await Visit.updateOne({ _id: visitId}, {$set: visit}) 
        } catch (err) {  
            log.logError('Error Getting the Page ID ' +  err.message)
            return next(err) 
        }   
    } 
    
    // Save Scroll Level On Heartbeat
    let behaviorScroll = new Behavior(
        { 
            visitId : visitId,
            event: "scrollOnDocument",
            timestamp: timestamp,
            element : "document",
            value : behaviors.articleInfos.scroll
        } 
    );

    try {
    behaviorScroll = await behaviorScroll.save()
    } catch (err) {
        
        log.logError("Error Saving Behavior Scroll On Document " + err.message) 
    }
    if (behaviors.articleInfos.pageClicks) {
    behaviors.articleInfos.pageClicks.map( async function (element,index)  { 
        let behavior = new Behavior(
            { 
                visitId : visitId,
                event: "pageClick",
                timestamp: timestamp,
                element : JSON.stringify(element.element),
                value : JSON.stringify({
                    windowX : element.docX,
                    windowY : element.docY,
                    parents : element.parents.map ( element => ( JSON.stringify(element)))
                }) 
            } 
        );
        try {
            await behavior.save()
        } catch (e) {
            log.logError("Error Saving Behavior Page Clicks " + err.message) 
        }
    })
    }

    if (behaviors.articleInfos.onFocus) {
        let behaviorOnFocus = new Behavior(
            { 
                visitId : visitId,
                event: "articleOnFocus",
                timestamp: timestamp,
                element : "article",
                value : behaviors.articleInfos.onFocus
            } 
        ); 
        try {
            behaviorOnFocus = await behaviorOnFocus.save()
        } catch (err) { 
            log.logError("Error Saving Behavior OnFocus On Article " + err.message) 
        }
    }


    behaviors.events.map( function (element,index)  {  
        element.events.map( async function (el) { 
            if ( el.event == "clicks" || el.event == "mousetrack" && el.value.length > 0 || el.event == "playHistory" )  
            el.value = el.value.map( value => (  JSON.stringify( value) ))

            let behavior = new Behavior(
                { 
                    visitId : visitId,
                    event: el.event,
                    timestamp: timestamp,
                    element : JSON.stringify(element.element),
                    value : el.value.toString()
                } 
            );
        
            try {
                await behavior.save()
            } catch (err) {

                log.logError("Error Saving Behavior Events " + err.message) 
          
            }

        })
 

    })
    res.json(behaviors);
};


exports.get_visit_scrolls = function (req,res,next) {
    Behavior.find({ visitId: req.params.visitid , event: "scrollOnDocument" , element : "document" } , function (err,behaviors) {
        if (err) { 
            res.json({error: err.message})
            return next(err);
        }
        res.json(behaviors);
    })
}


exports.get_visit_readtime = function (req,res,next) {
    Behavior.find({ visitId: req.params.visitid , event: "articleOnFocus" , element : "article" } , function (err,behaviors) {
        if (err) { 
            res.json({error: err.message})
            return next(err);
        }
        res.json(behaviors);
    })
}

exports.get_visit_playtime = function (req,res,next) {
    Behavior.find({ visitId: req.params.visitid , event: "playTime" } , function (err,behaviors) {
        if (err) { 
            res.json({error: err.message})
            return next(err);
        }
        res.json(behaviors);
    })
}


exports.get_behavior = function (req, res,next) {
    Behavior.findById(req.params.id, function (err, behavior) {
        if (err) { 
            res.json({error: err.message})
            return next(err);
        }
        res.json(behavior);
    })
};

exports.update_behavior = function (req, res,next) {
    Behavior.updateOne({ _id: req.params.id}, {$set: req.body}, function (err, behavior) {
        if (err) { 
            res.json({error: err.message})
            return next(err);
        }
        res.json(behavior);
    });
};

exports.deletevideosaudios = async function (req, res, next) {
    await Behavior.deleteMany({event :'playTime'})
    await Behavior.deleteMany({event :'playHistory'})
    res.json('DONE')
}

 
