var fs = require('fs')
 
class Logger { 
     constructor () {
        this.errors = fs.createWriteStream('logs/errors.txt', { flags: 'a'}) 
        this.warningslog = fs.createWriteStream('logs/warnings.txt', { flags: 'a'}) 
    }

    logError(logs) { 
        this.errors.write(this.getTime() +' '+logs+'\n')
    }  

    logWarning(logs) {
       this.warningslog.write(this.getTime() +' '+logs+'\n') 
    }
    
    getTime () {
        let currentdate = new Date()
        return  currentdate.getFullYear() + "/"
                + (currentdate.getMonth()+1)  + "/" 
                + currentdate.getDate() + " @ "  
                + currentdate.getHours() + ":"  
                + currentdate.getMinutes() + ":" 
                + currentdate.getSeconds() + " >>>>>> ";
    }
}



module.exports = Logger;
