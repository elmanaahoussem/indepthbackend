const Visit = require('../models/visit.model');
const User = require('../models/user.model');
const pageController = require('./page.controller')
const Logger = require('./Logger')

let log = new Logger () 

exports.add_visit = function (req, res,next) {   
    // Adding a Visit Creates/gets its user and creates/gets its page
    User.findOne({ cookieID : req.body.userid }, async function (err, user) {  
        if (err) { 
            log.logError("Finding USER findOne " + err.message) 
            return next(err) 
        } 
        if(!user) {  
            let userInstance = new User( { cookieID: req.body.userid, ip: req.body.ip , country : req.body.country  } ); 
            try {
                userInstance = await userInstance.save() 
                user = userInstance  
            } catch (err) {
                log.logError("Error Saving User " + err.message) 
                res.status(500)
                return next(err);  
            } 
        }
        if (!user.country)
        {
            try {
                user.country = req.body.country 
                await User.updateOne({ _id: user._id}, {$set: user}) 
            } catch (err) {
                log.logError('Error Updating User Country URL' + page.url +  " Message : " + err.message)
                return next(err) 
            }  
        }  
        let pageId 
        try {
        pageId = await pageController.getPageId(req.body.url,req.body.pagetype,req.body.pageauthors,req.body.pagepublishdate,req.body.pagetitle)
        } catch (err) {
                log.logError("Getting Page ID Error  " + err.message) 
                res.status(500)
                return next(err); 
        }  
        let visitInstance = new Visit(
            { 
                userId : user._id,
                pageId: pageId, 
                device: req.body.device,
                date : req.body.date   ,
                windowHeight : req.body.windowheight ,
                windowWidth : req.body.windowwidth , 
                referrer: req.body.referrer
            }
        );  
        visitInstance.save(function (err) {
            if (err) { 
                log.logError("Visit Instance Saving Error :  " + err.message) 
                res.status(500)
                return next(err);
            } 
            res.json(visitInstance)
        }) 
    }) 
};

exports.get_visit = function (req, res,next) {
    // Gets a Visit By Id
    Visit.findById(req.params.id, function (err, visit) {
        if (err) { 
            log.logError("Getting Visit Instance Error :  " + err.message) 
            res.status(500)
            return next(err);
        }
        res.json(visit);
    })
}
 
exports.get_all_visits = function ( req,res,next) { 
    // Gets all Visits
    Visit.find({},function (err,visits) {
        if (err) { 
            log.logError("Getting Visit Instance Error :  " + err.message) 
            res.status(500)
            return next(err);
        }
        res.json(visits);
    }) 
}


