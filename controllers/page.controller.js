const Page = require('../models/page.model');
const Scrapper = require('./Scrapper')
const fs = require('fs')
const path  = require('path')
const md5 = require('md5') 
const Logger = require('./Logger')

exports.getPageId = async function (url,pagetype,authors,publishdate,pagetitle) {
    // Returns Page Id to be used in visits creates one in case none found
    let log = new Logger ()  
    let pageScrapper = new Scrapper(url)
    let pageHash = await pageScrapper.generateSiteHash() 
    let date = new Date()
    let month = date.getMonth()  + 1 
        try {
            page = await Page.findOne({ url : url })
        } catch (err) {  
            log.logError('Error Getting the Page ID ' +  err.message)
            return next(err) 
        }  
        if( !page ) {   
            let pageInstance = null 
            let fileName =  'cache' + path.sep + md5( url + (new Date()).toString() ) +'.html'; 
            fs.writeFile(fileName, pageScrapper.currentHtml , function (err) {
                if (err) throw err; 
            });   
            try { 
                pageInstance = new Page( { 
                    url: url , 
                    hash : pageHash.trim() , 
                    versions : JSON.stringify( [{ version:0,date: date.getDate()+'-'+month+'-'+date.getFullYear() ,file: fileName }]),
                    publishdate : publishdate,
                    authors : authors , 
                    type : pagetype,
                    title : pagetitle
                } ) 
                pageInstance = await pageInstance.save()  
            } catch (err) {
                log.logError('Error Creating Page ' +  err.message) 
            }  
            page = pageInstance  
        } else { 
            if (page.hash != pageHash) 
            {    
                let fileName =  'cache' + path.sep + md5( url + (new Date()).toString() ) +'.html'; 
                fs.writeFile(fileName, pageScrapper.currentHtml , function (err) {
                    if (err) throw err; 
                }); 
                let versions = JSON.parse(page.versions)
                versions.push({ version : versions[versions.length -1].version +1 , date: date.getDate()+'-'+month+'-'+date.getFullYear() ,  file: fileName })
                
                page.versions = JSON.stringify(versions)
                page.hash = pageHash.trim() 
                try {
                     await Page.updateOne({ _id: page._id}, {$set: page}) 
                } catch (err) { 
                    log.logError('Error Updating Page URL' + page.url +  " Message : " + err.message)
                    return next(err) 
                }    
            }  
            // to fix Collected Data (Temp Scripts)
            
                page.authors = authors
                page.title = pagetitle
                try {
                    await Page.updateOne({ _id: page._id}, {$set: page}) 
                } catch (err) {
                    log.logError('Error Updating Page URL' + page.url +  " Message : " + err.message)
                    return next(err) 
                } 
        }  
    return page._id 
};
 