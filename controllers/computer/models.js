
exports.createDailyHitsSchema = async function (MongoInstance) { 
    const Schema = MongoInstance.Schema;
    let DailyHits = new Schema({ 
        startDate : { type: Date },
        endDate : { type: Date },
        dayNumber : {type : Number},
        pageId : { type : MongoInstance.Schema.Types.ObjectId }, 
        title: { type: String }, 
        publishDate : { type: Date },
        authors: { type: String },
        type: { type: String },
        versions : { type : String },
        scopes : { type: String } 
    });
    return MongoInstance.model('DailyHits', DailyHits);
}

