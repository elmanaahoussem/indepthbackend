const models = require('./models')
const Mongoose = require('mongoose').Mongoose; 


class ScopeCalculator  {

    async init() {
        
        var queryBase = new Mongoose();
        var scopeBase = new Mongoose(); 

        this.queryBase = await queryBase.connect('mongodb://localhost:27017/indepth',{useNewUrlParser: true,useUnifiedTopology: true})    
        this.scopeBase = await scopeBase.connect('mongodb://localhost:27017/indepthscopes',{useNewUrlParser: true,useUnifiedTopology: true})
    }

    makeModels () {
        
    }
 
    async insertHit (hitType,record) {
        try {
            switch (hitType) {
                case 'daily' : 
                    await this.scopeBase.connection.collection('dailyHits').insertOne(record)  
                    break
                case 'weekly' : 
                    await this.scopeBase.connection.collection('weeklyHits').insertOne(record)
                    break
                case 'monthly' : 
                    await this.scopeBase.connection.collection('monthlyHits').insertOne(record)
                    break
                case 'yearly' :
                    await this.scopeBase.connection.collection('yearlyHits').insertOne(record)
                    break
            } 
        }catch (e) {
            console.log('-----------ERRRORRRRR-------------- :: ' , e.message)
        }
    }

    /*
    deleteCurrentTables ()

    calculateDailyHits (startDate,endDate,urls = null)
    calculateWeeklyHits (startDate,endDate,urls = null)
    calculateMonthlyHits (startDate,endDate,urls = null) 
    calculateYearlyHits (startDate,endDate,urls = null)
    */


}

module.exports = ScopeCalculator ;