const cheerio = require('cheerio');
const request = require('request-promise');
const encoder = require('./Encoder')
const md5 = require('md5')
const Logger = require('./Logger')

class Scrapper { 

    constructor (WebUrl) { 
        this.WebUrl = WebUrl 
        this.currentHtml = null
        this.skeleton = null 
        this.log = new Logger()
    } 
    async generateSiteHash () { 
        // Generates a Hash For the Page Skeleton 
        let siteSkeleton =  await this.requestPageSkeleton()  
        return md5(JSON.stringify(siteSkeleton)) 
    }
  
    injectScript (html) { 
        // Injects a the Data-track on every Node in the page
        let idx = html.length - 15
        let rem = 0
        let script = '<script> document.addEventListener("DOMContentLoaded", function(){ const siteElements  = $("body *"); var number = 0 ; siteElements.map(function (k) {  $(this).attr("data-track", number++)   }) })</script>' 
        return html.slice(0, idx) + script + html.slice(idx + Math.abs(rem)); 
    } 
    
    async requestPageSkeleton (URL = this.WebUrl) {
        // Send Request to get The Website and gets the Body Skeleton 
        var options = { 
            uri: decodeURI(URL),
            transform: (html) => { 
                this.currentHtml = this.injectScript(html)
                const Skeleton = this.extractSkeleton(html)
                this.skeleton =  Skeleton 
                return Skeleton
            }
        }
        try {
            return await request(options)   
        } catch (e) { 
            this.log.logError('Error Request Skeleton  URL IS : ' + decodeURI(URL) + ' Message Errror is : ' +  e.message) 
            return Promise.reject(decodeURI(URL) +' Request Page Skeleton Failed')
        } 
    }

    extractSkeleton (html) {
        // Return Page Skeleton as Array
        const $ = cheerio.load(html);  
        const siteElements  = $("body *"); 
        let siteStructure = [];  
        let number = 0   
        siteElements.map(function (k) { 
            siteStructure[k] = new Object;
            siteStructure[k].name =  $(this).prop("tagName")
            siteStructure[k].class =  $(this).attr("class")
            siteStructure[k].number =  number++
        })
        return siteStructure  
    } 

}

module.exports = Scrapper