const loadroutes = require('./routes/routes.loader'); 
const app = loadroutes()  
const fs = require('fs')
const server = require('https') 
var cors = require('cors')

app.use(cors())

const appPort = process.env.PORT || 3000

const mongoose = require('mongoose');
// localhost - mongo  mongodb://51.210.179.152:27019/indepth  
mongoose.connect('mongodb://51.210.179.152:27019/indepth',{useNewUrlParser: true,useUnifiedTopology: true})   
    .then(() => {
        console.log('Conntected To Mongodb')
        app.listen(appPort, () => {
            console.log('Server is up and running on port number ' + appPort);  
        });
    })   
    .catch(err => console.error('Connection failed...' + appPort));
