const express = require('express');
const router = express.Router();
 
const visit_controller = require('../controllers/visit.controller');
 
router.post('/add', visit_controller.add_visit); 
router.all('/all', visit_controller.get_all_visits); 
router.get('/:id', visit_controller.get_visit);

module.exports = router;



