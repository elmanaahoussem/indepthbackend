const express = require('express');
const router = express.Router();
 
const behavior_controller = require('../controllers/behavior.controller');
 
router.post('/add', behavior_controller.add_behavior);

router.get('/:visitid/scrolls',behavior_controller.get_visit_scrolls)
router.get('/:visitid/readTime',behavior_controller.get_visit_readtime)
router.get('/:visitid/playtime',behavior_controller.get_visit_playtime)
//router.get('/deletevideosaudios',behavior_controller.deletevideosaudios)
//router.get('/:visitid/full',behavior_controller.get_visit_full)

router.get('/:id', behavior_controller.get_behavior);
 
 
module.exports = router;



