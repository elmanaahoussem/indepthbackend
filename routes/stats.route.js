const express = require('express');
const router = express.Router();
 
const stats_controller = require('../controllers/stats.controller');
 
router.get('/getpagelist',stats_controller.get_pagelist)   

router.get('/getstats',stats_controller.get_stats)
router.get('/apitest',stats_controller.apiTest)
router.get('/fixipvisit',stats_controller.fixipvisit)

module.exports = router;
