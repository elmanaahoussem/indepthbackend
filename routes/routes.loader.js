const express = require('express'); 
const bodyParser = require('body-parser');

const behavior = require('./behavior.route');  
const visit = require('./visit.route');
const user = require('./user.route');    
const stats = require('./stats.route');    
module.exports = function loadroutes () {
    const app = express(); 
    app.use(function(req, res, next) {
        res.set("Access-Control-Allow-Origin", "*"); 
        res.set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        next();
      });
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({extended: false}));

    
    app.use('/behavior', behavior); 
    app.use('/visit', visit); 
    app.use('/user', user); 
    app.use('/stats', stats); 

    return app
}
